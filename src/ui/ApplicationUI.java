package ui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import dao.ManifestacijaDAO;
import utils.ScannerWrapper;

public class ApplicationUI {

	public static Connection conn;
	public static ManifestacijaDAO manDAO = new ManifestacijaDAO();
	
	static {
		try {
			// ucitavanje MySQL drajvera
			Class.forName("com.mysql.jdbc.Driver");

			// konekcija
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/manifestacije", "root", "root");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
//meni osnovne opcije	
	public static void main(String[] args)  {
		int odluka = -1;
		while (odluka != 0) {
			ApplicationUI.ispisiMenu();
			System.out.print("opcija:");
			odluka = ScannerWrapper.ocitajCeoBroj();
			
			switch (odluka) {
			case 0:
				System.out.println("Izlaz iz programa");
				break;
			case 1:
				ManifestacijaUI.ispisiSveManifestacije();
				break;
			case 2:
				GradUI.ispisiSveGradove();
				break;
			case 3:
				System.out.println(ManifestacijaUI.pronadjiMan());
				break;
			case 4:
				ManifestacijaUI.unosNoveManifestacije();
				break;
			case 5:
				ManifestacijaUI.izmenaPodatakaOManifestaciji();
				break;
			case 6:
				ManifestacijaUI.brisanjePodatakaOManifestaciji();
				break;
			case 7:
				GradUI.brisanjePodatakaOGradu();
				break;
			case 8:
				GradUI.izmenaPodatakaOGradu();
				break;
			case 9:
				ManifestacijaUI.upisUFajl();
				ManifestacijaUI.citanjeIzFajla();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}	
// ispis teksta osnovnih opcija
	public static void ispisiMenu() {
		System.out.println("Manifestacije Srbije - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - Prikaz svih manifestacija");
		System.out.println("\tOpcija broj 2 - Prikaz svih gradova");
		System.out.println("\tOpcija broj 3 - Pretraga manifestacija po ID broju");
		System.out.println("\tOpcija broj 4 - Unos manifestacije");
		System.out.println("\tOpcija broj 5 - Izmena manifestacije");
		System.out.println("\tOpcija broj 6 - Brisanje manifestacije");
		System.out.println("\tOpcija broj 7 - Brisanje grada");
		System.out.println("\tOpcija broj 8 - Izmena grada");
		System.out.println("\tOpcija broj 9 - Manifestacija sa najvecom posecenoscu");
		
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
	}
}




