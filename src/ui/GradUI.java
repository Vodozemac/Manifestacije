package ui;

import java.util.List;

import utils.ScannerWrapper;
import model.Grad;
import dao.GradDAO;

public class GradUI {

	public static GradDAO gradDAO = new GradDAO();
		
// ispisi sve gradove
	public static void ispisiSveGradove() {
		List<Grad> sviGradovi = gradDAO.getAll(ApplicationUI.conn);			
		for (Grad a : sviGradovi) {
			System.out.println(a);
			}
	}	
// pronadji grad
	public static Grad pronadjiGrad() {
		Grad retVal = null;
		System.out.print("Unesi PTT grada:");
		int ptt_grad = ScannerWrapper.ocitajCeoBroj();
		retVal = pronadjiGrad(ptt_grad);
		
		if (retVal == null){
			System.out.println("***GRAD SA UNETIM PTT NE POSTOJI U EVIDENCIJI.***");
		}
		return retVal;
	}
// pronadji grad
	public static Grad pronadjiGrad(int ptt_grad) {
		Grad retVal = gradDAO.getGradByPtt(ApplicationUI.conn, ptt_grad);
		return retVal;
	}	
// izmena grada, menja se samo naziv
	public static void izmenaPodatakaOGradu() {
		Grad grad = pronadjiGrad();
		if(grad != null){
			System.out.print("Unesi novi naziv grada :");
			String noviNaziv = ScannerWrapper.ocitajTekst();
			grad.setNaziv(noviNaziv);
				
			GradUI.gradDAO.update(ApplicationUI.conn, grad);
				if(GradUI.gradDAO.update(ApplicationUI.conn, grad) == true){
					System.out.println("***USPESNA IZMENA***");
				}else{
					System.out.println("***GRESKA***");
				}
		} 
	}
//brisanje grada
	public static void brisanjePodatakaOGradu(){
		Grad grad = pronadjiGrad();
		if(grad != null){
			GradUI.gradDAO.delete(ApplicationUI.conn, grad.getPtt());
			System.out.println("***USPESNO BRISANJE***");
		}
	}	
}
