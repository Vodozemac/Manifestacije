package ui;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import model.Grad;
import model.Manifestacija;
import dao.ManifestacijaDAO;
import utils.ScannerWrapper;

public class ManifestacijaUI {
	
	public static ManifestacijaDAO manDAO = new ManifestacijaDAO();

// ispisi sve MANIFESTACIJE
	public static void ispisiSveManifestacije() {
		List<Manifestacija> sveManifestacije = manDAO.getAll(ApplicationUI.conn);
			
		for (Manifestacija m : sveManifestacije) {
			System.out.println(m);
		}
	}

// pronadji manifestaciju
		public static Manifestacija pronadjiMan(){
			Manifestacija retVal = null;
			System.out.print("Unesi ID manifestacije:");
			int id_man = ScannerWrapper.ocitajCeoBroj();
			retVal = pronadjiMan(id_man);
			if (retVal == null){
				System.out.println("***MANIFESTACIJA SA UNETIM ID NE POSTOJI U EVIDENCIJI.***");
			}
			return retVal;
		}

// pronadji manifestaciju
		public static Manifestacija pronadjiMan(int id_man) {
			Manifestacija retVal = manDAO.getManifestacijaById(ApplicationUI.conn, id_man);
			return retVal;
		}	
		
// unos nove manif.
	public static void unosNoveManifestacije() {
		int id_manifestacija = -1;
		System.out.print("Unesi naziv manifestacije:");
		String naziv = ScannerWrapper.ocitajTekst();
		System.out.print("Unesi broj posetilaca:");
		int broj_posetilaca = ScannerWrapper.ocitajCeoBroj();
		System.out.print("Unesi PTT grada:");
		int ptt = ScannerWrapper.ocitajCeoBroj();
		Grad grad = GradUI.pronadjiGrad(ptt);
			
		if(grad == null){
			System.out.println("***GRAD SA UNESENIM PTT NE POSTOJI U EVIDENCIJI.***");
			return;
		}

		Manifestacija m = new Manifestacija(id_manifestacija, naziv, broj_posetilaca, grad);
		ManifestacijaUI.manDAO.add(ApplicationUI.conn, m);
		System.out.println("***USPESAN UNOS***");
	}	
// izmena manifestacije
	public static void izmenaPodatakaOManifestaciji() {
		Manifestacija m = pronadjiMan();
		if(m != null){
			int choice; 
			do{
				System.out.println(m);
				System.out.println("Izmena manifestacije - opcije:");
				System.out.println("\tOpcija broj 1 - naziva");
				System.out.println("\tOpcija broj 2 - broj posetilaca");
				System.out.println("\tOpcija broj 3 - grad (PTT)");
				System.out.println("\t\t ...");
				System.out.println("\tOpcija broj 0 - IZLAZ");
				System.out.print("opcija: ");
				choice = ScannerWrapper.ocitajCeoBroj();
				switch (choice) {
					case 1:
						System.out.print("Unesi novi naziv :");
						String naziv = ScannerWrapper.ocitajTekst();
						m.setNaziv(naziv);
						break;
					case 2:
						System.out.print("Unesi novi broj posetilaca :");
						int br_poset = ScannerWrapper.ocitajCeoBroj();
						m.setBroj_posetilaca(br_poset);
						break;
					case 3:
						System.out.print("Unesi novi PTT grada u kom je manifestacija odrzana :");
						int ptt = ScannerWrapper.ocitajCeoBroj();
						Grad grad = GradUI.pronadjiGrad(ptt);

						if(grad == null){
							System.out.println("GRAD SA UNETIM PTT NE POSTOJI U EVIDENCIJI.");
							return;
						}					
						
						m.setGrad(grad);
						break;
				}
			ManifestacijaUI.manDAO.update(ApplicationUI.conn, m);
			if(ManifestacijaUI.manDAO.update(ApplicationUI.conn, m) == true){
				System.out.println("***USPESNA IZMENA***");
			}else{
					System.out.println("***GRESKA***");
			}
		} while(choice != 0);
		}
	}
	
//brisanje manif.
	public static void brisanjePodatakaOManifestaciji(){
		Manifestacija m = pronadjiMan();
		if(m != null){
			ManifestacijaUI.manDAO.delete(ApplicationUI.conn, m.getId_manifestacija());
			System.out.println("***USPESNO BRISANJE***");
		}
	}
	
//upis u fajl	
	public static void upisUFajl(){		
		try {
			String tekst = manDAO.maxPosetilaca(ApplicationUI.conn);
			
			File file = new File("data\\manifestacije.csv");
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(tekst);
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
//citanje iz fajla
	public static void citanjeIzFajla(){
				
		try (BufferedReader br = new BufferedReader(new FileReader("data\\manifestacije.csv")))
		{
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				System.out.println(sCurrentLine);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} 		
	}		
}
