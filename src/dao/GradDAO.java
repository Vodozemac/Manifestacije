package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Grad;

public class GradDAO {
//pronadji grad po ptt-u	
	public Grad getGradByPtt(Connection conn, int ptt) {
		Grad grad = null;
		
		try {
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt.executeQuery("select ptt, naziv from gradovi where ptt =" + ptt);

			if (rset.next()) {
				int ptt_grad = rset.getInt(1);
				String naziv = rset.getString(2);
				
				grad = new Grad(naziv, ptt_grad);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return grad;
	}
//lista svih gradova	
	public List<Grad> getAll(Connection conn) {
		List<Grad> retVal = new ArrayList<Grad>();
		try {
			String query = "select ptt, naziv from gradovi";
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt.executeQuery(query.toString());
			
			while (rset.next()) {
				int ptt_grad = rset.getInt(1);
				String naziv = rset.getString(2);
				
				Grad grad = new Grad(naziv, ptt_grad);
				retVal.add(grad);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}
//izmena naziva grada	
	public boolean update(Connection conn, Grad grad) {
		boolean retVal = false;
		try {
			String update = "UPDATE gradovi SET naziv=? WHERE ptt =?";
			PreparedStatement pstmt = conn.prepareStatement(update);
			pstmt.setString(1, grad.getNaziv());
			pstmt.setInt(2, grad.getPtt());
			
		if(pstmt.executeUpdate() == 1)
			retVal = true;
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
//brisanje grada	
	public boolean delete(Connection conn, int ptt) {
		boolean retVal = false;
		try {
			String update = "DELETE FROM gradovi WHERE ptt = " + ptt;
			Statement stmt = conn.createStatement();
			if (stmt.executeUpdate(update) == 1)
				retVal = true;
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
}
