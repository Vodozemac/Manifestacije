package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ui.GradUI;
import model.Grad;
import model.Manifestacija;

public class ManifestacijaDAO {
	
//pretraga manifestacija po ID	
	public Manifestacija getManifestacijaById(Connection conn, int id) {
		Manifestacija m = null;
		try {
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt.executeQuery("select id, naziv_manifestacije, broj_posetilaca, ptt_grad from manifestacije where id =" + id);
			if (rset.next()) {
				int id_manifestacija = rset.getInt(1);
				String naziv = rset.getString(2);
				int broj_posetilaca = rset.getInt(3);
				Grad grad = GradUI.pronadjiGrad(rset.getInt(4));
					
				m = new Manifestacija(id_manifestacija, naziv, broj_posetilaca, grad);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return m;
	}
//lista svih manifestacija	
	public List<Manifestacija> getAll(Connection conn) {
		List<Manifestacija> retVal = new ArrayList<Manifestacija>();
		try {
			String query = "select id, naziv_manifestacije, broj_posetilaca, ptt_grad from manifestacije";
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt.executeQuery(query.toString());
				
			while (rset.next()) {
				int id_manifestacija = rset.getInt(1);
				String naziv = rset.getString(2);
				int broj_posetilaca = rset.getInt(3);
				Grad grad = GradUI.pronadjiGrad(rset.getInt(4));
					
				Manifestacija m = new Manifestacija(id_manifestacija, naziv, broj_posetilaca, grad);
				retVal.add(m);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}
//unos nove manifestacije
	public boolean add(Connection conn, Manifestacija m){
		boolean retVal = false;
		try {
			String update = "insert into manifestacije (naziv_manifestacije, broj_posetilaca, ptt_grad) values (?, ?, ?)";
			PreparedStatement pstmt = conn.prepareStatement(update);
			pstmt.setString(1, m.getNaziv());
			pstmt.setInt(2, m.getBroj_posetilaca());
			pstmt.setInt(3, m.getGrad().getPtt());
				
			if(pstmt.executeUpdate() == 1){
				retVal = true;
			}
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
//izmena manifestacije	
	public boolean update(Connection conn, Manifestacija m) {
		boolean retVal = false;
		try {
			String update = "UPDATE manifestacije SET naziv_manifestacije= ?, broj_posetilaca=?, ptt_grad=? WHERE id = ?";
			PreparedStatement pstmt = conn.prepareStatement(update);
			pstmt.setString(1, m.getNaziv());
			pstmt.setInt(2, m.getBroj_posetilaca());
			pstmt.setInt(3, m.getGrad().getPtt());
			pstmt.setInt(4, m.getId_manifestacija());
			
		if(pstmt.executeUpdate() == 1)
			retVal = true;
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
//brisanje manifestacije	
	public boolean delete(Connection conn, int id) {
		boolean retVal = false;
		try {
			String update = "DELETE FROM manifestacije WHERE id = " + id;
			Statement stmt = conn.createStatement();
			if (stmt.executeUpdate(update) == 1)
				retVal = true;
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
//manifestacija sa najvise posetilaca
	public String maxPosetilaca(Connection conn) {
		String max = "";
		try {
			String query = "select id, naziv_manifestacije, broj_posetilaca, ptt_grad from" +
					" manifestacije left join gradovi on  manifestacije.ptt_grad = gradovi.ptt" +
					" where broj_posetilaca = (select max(broj_posetilaca) from manifestacije)"; 
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt.executeQuery(query.toString());
				
			while (rset.next()) {
				int id_manifestacija = rset.getInt(1);
				String naziv = rset.getString(2);
				int broj_posetilaca = rset.getInt(3);
				Grad grad = GradUI.pronadjiGrad(rset.getInt(4));
					
				Manifestacija m = new Manifestacija(id_manifestacija, naziv, broj_posetilaca, grad);
				max = m.toString();
			}			
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return max;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
