package model;

public class Grad {

	String naziv;
	int ptt;
//konstruktori	
	public Grad(String naziv, int ptt) {
		
		this.naziv = naziv;
		this.ptt = ptt;
	}

	public Grad() {
	}
//get-set
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public int getPtt() {
		return ptt;
	}
	public void setPtt(int ptt) {
		this.ptt = ptt;
	}
//toString
	public String toString() {
		return naziv;
	}	
}
