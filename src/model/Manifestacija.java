package model;

public class Manifestacija {

	int id_manifestacija;
	String naziv;
	int broj_posetilaca;
	Grad grad;
//konstruktori	
	public Manifestacija(int id_manifestacija, String naziv,
			int broj_posetilaca, Grad grad) {
		
		this.id_manifestacija = id_manifestacija;
		this.naziv = naziv;
		this.broj_posetilaca = broj_posetilaca;
		this.grad = grad;
	}

	public Manifestacija() {
	}
//get-set
	public int getId_manifestacija() {
		return id_manifestacija;
	}

	public void setId_manifestacija(int id_manifestacija) {
		this.id_manifestacija = id_manifestacija;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getBroj_posetilaca() {
		return broj_posetilaca;
	}

	public void setBroj_posetilaca(int broj_posetilaca) {
		this.broj_posetilaca = broj_posetilaca;
	}

	public Grad getGrad() {
		return grad;
	}

	public void setGrad(Grad grad) {
		this.grad = grad;
	}
//toString
	public String toString() {
		return "Manifestacija ID: " + id_manifestacija	+ ", " + naziv + ", broj posetilaca: " + broj_posetilaca
				+ ", grad: " + grad;
	}
	
//formatiranje teksta za upis u fajl	
	public String toFileRepresentation(){
		//spaja sve podatke u jednu liniju
		StringBuilder bild = new StringBuilder(); 
		bild.append(naziv + ", " + broj_posetilaca);	
				
			return bild.toString(); //od linije pravi string
	}
	
	
	
}
