-- brisanje tabela ukoliko postoje
drop table if exists manifestacije;
drop table if exists gradovi;
 
-- kreiranje tabela 
create table gradovi (

	ptt int  not null unique,
	naziv varchar(45),
    
    primary key(ptt)
);

create table manifestacije (

	id int auto_increment,
	naziv_manifestacije varchar(70),
    broj_posetilaca int not null,
    ptt_grad int,
    
    primary key(id),
    FOREIGN KEY (ptt_grad)
						REFERENCES gradovi(ptt)
						ON DELETE RESTRICT
);
-- unos vrednosti
insert into gradovi (ptt, naziv) values (32000, 'Cacak');
insert into gradovi (ptt, naziv) values (24000, 'Subotica');
insert into gradovi (ptt, naziv) values (11000, 'Beograd');
insert into gradovi (ptt, naziv) values (23000, 'Zrenjanin');
insert into gradovi (ptt, naziv) values (15000, 'Loznica');
insert into gradovi (ptt, naziv) values (21000, 'Novi Sad');
insert into gradovi (ptt, naziv) values (31000, 'Uzice');

insert into manifestacije (naziv_manifestacije, broj_posetilaca, ptt_grad) values ('Tradicionalni izlazak na vrh Kablara', 2867, 32000);
insert into manifestacije (naziv_manifestacije, broj_posetilaca, ptt_grad) values ('Winter fest', 10867, 24000);
insert into manifestacije (naziv_manifestacije, broj_posetilaca, ptt_grad) values ('Međunarodni susreti Oldtimer vozila', 7817, 24000);
insert into manifestacije (naziv_manifestacije, broj_posetilaca, ptt_grad) values ('Ulica otvorenog srca', 15011, 11000);
insert into manifestacije (naziv_manifestacije, broj_posetilaca, ptt_grad) values ('Guitar art festival', 22018, 11000);
insert into manifestacije (naziv_manifestacije, broj_posetilaca, ptt_grad) values ('Belgrade fashion week', 27318, 11000);
insert into manifestacije (naziv_manifestacije, broj_posetilaca, ptt_grad) values ('Dani piva', 53318, 23000);
insert into manifestacije (naziv_manifestacije, broj_posetilaca, ptt_grad) values ('Banatske vredne ruke', 2318, 23000);
insert into manifestacije (naziv_manifestacije, broj_posetilaca, ptt_grad) values ('Gradska smotra recitatora', 5799, 15000);
insert into manifestacije (naziv_manifestacije, broj_posetilaca, ptt_grad) values ('Međunarodna Drinska regata', 20318, 15000);
insert into manifestacije (naziv_manifestacije, broj_posetilaca, ptt_grad) values ('EXIT', 298126, 21000);
insert into manifestacije (naziv_manifestacije, broj_posetilaca, ptt_grad) values ('Antićevi dani', 5999, 21000);
insert into manifestacije (naziv_manifestacije, broj_posetilaca, ptt_grad) values ('Sterijino pozorje', 22318, 21000);
insert into manifestacije (naziv_manifestacije, broj_posetilaca, ptt_grad) values ('Svetosavski bal', 12361, 31000);

